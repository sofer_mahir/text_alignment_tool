from text_alignment_tool import (
    TextAlignmentTool,
    NewlineSeparatedTextLoader,
    BracketingPair,
    RemoveCharacter,
    CharacterSubstitution,
    MultipleCharacterSubstitution,
    TextTransformer,
    ConsistentBracketingTransformer,
    RemoveCharacterTransformer,
    RemoveEnclosedTransformer,
    SubstituteCharacterTransformer,
    SubstituteMultipleCharactersTransformer,
)
from text_alignment_tool.text_transformers.text_transformer import TextTransformer
from .helper_utils import pairwise_equal_array
import pytest


bracket_transformer = (
    "I have in[consistent[ \nbra]ck]eting",
    "I have in[consistent] \nbra[ck]eting",
    ConsistentBracketingTransformer([BracketingPair("[", "]")]),
)

remove_character = (
    "#I have @@wierd@@ charac!!ters#",
    "I have wierd characters",
    RemoveCharacterTransformer(
        [RemoveCharacter("@"), RemoveCharacter("#"), RemoveCharacter("!")]
    ),
)

remove_enclosed = (
    "I should not include [words to be deleted] here",
    "I should not include  here",
    RemoveEnclosedTransformer("[", "]"),
)

substitute_character = (
    "I should not be less excited!",
    "I should not be less excited.",
    SubstituteCharacterTransformer([CharacterSubstitution("!", ".")]),
)

substitute_multiple_characters = (
    "I should———not be—so excited!",
    "I should—not be—so excited.",
    SubstituteMultipleCharactersTransformer(
        [
            MultipleCharacterSubstitution("———", "—"),
            MultipleCharacterSubstitution("!", "."),
        ]
    ),
)


@pytest.mark.parametrize(
    "query_str,target_str,transformer",
    [
        bracket_transformer,
        remove_character,
        remove_enclosed,
        substitute_character,
        substitute_multiple_characters,
    ],
)
def test_transformers(query_str: str, target_str: str, transformer: TextTransformer):
    query = NewlineSeparatedTextLoader(query_str)
    target = NewlineSeparatedTextLoader(target_str)
    aligner = TextAlignmentTool(query, target)
    aligner.query_text_transform(transformer)
    pair_equal, msg = pairwise_equal_array(target.output, transformer.output)
    assert pair_equal, msg
