# TODO: add tests for all other alignment algorithms

from text_alignment_tool import (
    TextAlignmentTool,
    AlignmentAlgorithm,
    TextLoader,
    AlignmentKey,
    NewlineSeparatedTextLoader,
    NeedleInHaystackAlgorithm,
)
from typing import List
import pytest

needle_in_haystack = (
    NewlineSeparatedTextLoader(
        """Cursus eget nunc scelerisque viverra mauris in aliquam."""
    ),
    NewlineSeparatedTextLoader(
        """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed libero enim sed faucibus turpis. Faucibus et molestie ac feugiat. Morbi tempus iaculis urna id volutpat lacus laoreet. Ultrices in iaculis nunc sed augue lacus. Vel facilisis volutpat est velit. Semper viverra nam libero justo laoreet. Aliquet porttitor lacus luctus accumsan tortor posuere. Varius sit amet mattis vulputate. Adipiscing commodo elit at imperdiet dui accumsan sit. Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Et ultrices neque ornare aenean euismod elementum nisi. Scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Ut tellus elementum sagittis vitae. Faucibus purus in massa tempor nec feugiat nisl. Sit amet venenatis urna cursus eget.

Mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Risus in hendrerit gravida rutrum quisque non tellus. Nulla pharetra diam sit amet nisl suscipit adipiscing. Porta non pulvinar neque laoreet suspendisse. Porta nibh venenatis cras sed felis eget velit. Nisl vel pretium lectus quam id. Luctus accumsan tortor posuere ac ut consequat semper. Mi proin sed libero enim sed faucibus turpis in. Maecenas sed enim ut sem viverra. Ut eu sem integer vitae justo eget. Nec ultrices dui sapien eget mi proin sed. Tincidunt id aliquet risus feugiat in ante. Orci nulla pellentesque dignissim enim sit amet venenatis. Consectetur adipiscing elit duis tristique sollicitudin nibh sit amet. Tristique risus nec feugiat in fermentum. Ut etiam sit amet nisl purus in mollis nunc sed. Vestibulum lorem sed risus ultricies tristique nulla aliquet enim tortor. Vestibulum morbi blandit cursus risus at ultrices mi. Non tellus orci ac auctor augue mauris augue neque gravida.

Porttitor lacus luctus accumsan tortor posuere ac. Aenean sed adipiscing diam donec adipiscing tristique. Urna condimentum mattis pellentesque id nibh tortor id aliquet. Morbi quis commodo odio aenean. Volutpat ac tincidunt vitae semper quis lectus nulla at. Amet dictum sit amet justo donec enim diam. Est ullamcorper eget nulla facilisi etiam. Velit laoreet id donec ultrices. Ac feugiat sed lectus vestibulum mattis ullamcorper velit sed. At urna condimentum mattis pellentesque id nibh. Cursus eget nunc scelerisque viverra mauris in aliquam. Accumsan sit amet nulla facilisi morbi tempus iaculis urna id. Viverra tellus in hac habitasse.

Aliquam purus sit amet luctus venenatis lectus magna fringilla. Praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Pretium viverra suspendisse potenti nullam ac tortor vitae purus. In massa tempor nec feugiat nisl pretium fusce. Et molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit. Nulla facilisi etiam dignissim diam quis enim lobortis. Mi bibendum neque egestas congue. Aliquam purus sit amet luctus venenatis lectus magna fringilla. Dignissim sodales ut eu sem integer vitae justo eget. Mauris rhoncus aenean vel elit scelerisque mauris. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Senectus et netus et malesuada fames ac turpis. Purus in massa tempor nec feugiat nisl. Dui sapien eget mi proin sed libero enim sed faucibus.

Duis ut diam quam nulla porttitor massa. Diam maecenas sed enim ut sem. Etiam sit amet nisl purus in. Non pulvinar neque laoreet suspendisse. Sagittis purus sit amet volutpat consequat. Placerat orci nulla pellentesque dignissim enim sit. Turpis massa sed elementum tempus. Aliquam ultrices sagittis orci a scelerisque purus semper eget duis. Eu ultrices vitae auctor eu augue ut lectus. Est ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Scelerisque eu ultrices vitae auctor eu. Nisi vitae suscipit tellus mauris a diam. Volutpat maecenas volutpat blandit aliquam etiam erat velit. Nibh tellus molestie nunc non blandit massa enim nec dui. Neque volutpat ac tincidunt vitae semper quis lectus nulla at. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu. Feugiat in ante metus dictum at tempor commodo. Aliquam sem et tortor consequat id. Ultricies lacus sed turpis tincidunt. Ultrices sagittis orci a scelerisque purus semper eget duis at."""
    ),
    [
        AlignmentKey(0, 2377),
        AlignmentKey(1, 2378),
        AlignmentKey(2, 2379),
        AlignmentKey(3, 2380),
        AlignmentKey(4, 2381),
        AlignmentKey(5, 2382),
        AlignmentKey(6, 2383),
        AlignmentKey(7, 2384),
        AlignmentKey(8, 2385),
        AlignmentKey(9, 2386),
        AlignmentKey(10, 2387),
        AlignmentKey(11, 2388),
        AlignmentKey(12, 2389),
        AlignmentKey(13, 2390),
        AlignmentKey(14, 2391),
        AlignmentKey(15, 2392),
        AlignmentKey(16, 2393),
        AlignmentKey(17, 2394),
        AlignmentKey(18, 2395),
        AlignmentKey(19, 2396),
        AlignmentKey(20, 2397),
        AlignmentKey(21, 2398),
        AlignmentKey(22, 2399),
        AlignmentKey(23, 2400),
        AlignmentKey(24, 2401),
        AlignmentKey(25, 2402),
        AlignmentKey(26, 2403),
        AlignmentKey(27, 2404),
        AlignmentKey(28, 2405),
        AlignmentKey(29, 2406),
        AlignmentKey(30, 2407),
        AlignmentKey(31, 2408),
        AlignmentKey(32, 2409),
        AlignmentKey(33, 2410),
        AlignmentKey(34, 2411),
        AlignmentKey(35, 2412),
        AlignmentKey(36, 2413),
        AlignmentKey(37, 2414),
        AlignmentKey(38, 2415),
        AlignmentKey(39, 2416),
        AlignmentKey(40, 2417),
        AlignmentKey(41, 2418),
        AlignmentKey(42, 2419),
        AlignmentKey(43, 2420),
        AlignmentKey(44, 2421),
        AlignmentKey(45, 2422),
        AlignmentKey(46, 2423),
        AlignmentKey(47, 2424),
        AlignmentKey(48, 2425),
        AlignmentKey(49, 2426),
        AlignmentKey(50, 2427),
        AlignmentKey(51, 2428),
        AlignmentKey(52, 2429),
        AlignmentKey(53, 2430),
        AlignmentKey(54, 2431),
    ],
    NeedleInHaystackAlgorithm(),
)


@pytest.mark.parametrize(
    "query_text,target_text,expected_indices,algorithm", [needle_in_haystack]
)
def test_alignment_algorithms(
    query_text: TextLoader,
    target_text: TextLoader,
    expected_indices: List[AlignmentKey],
    algorithm: AlignmentAlgorithm,
):
    aligner = TextAlignmentTool(query_text, target_text)
    aligner.align_text(algorithm)
    algorithm_alignment = algorithm.align().alignments

    longest, shortest = (
        (algorithm_alignment, expected_indices)
        if len(algorithm_alignment) > len(expected_indices)
        else (expected_indices, algorithm_alignment)
    )
    for idx, (current_pair, expected_pair) in enumerate(zip(longest, shortest)):
        msg = f"Current index {current_pair} and expected index {expected_pair} at position {idx} in the alignment"
        assert current_pair.query_idx == expected_pair.query_idx, msg
        assert current_pair.target_idx == expected_pair.target_idx, msg
    assert len(longest) == len(shortest)
    pass
