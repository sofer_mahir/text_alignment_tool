import numpy as np

l1 = [0, 100, 22, 30789, 4]
l2 = [4, 30789, 22, 100, 0]
l3 = [1,2,3,4]

max_size = 2**32
def int_array_hash(l: list[int]) -> int:
    acc = 1
    for a, b in zip(l, l[-1:] + l[:-1]):
        acc += (acc * a - b)
    return (acc + max_size) % max_size

def hash2(l: list[int]) -> int:
    return np.bitwise_xor.reduce(l)

print(hash(l1))
print(hash(l2))
print(hash(l3))

# print(hash2(l1))
# print(hash2(l2))
# print(hash2(l3))